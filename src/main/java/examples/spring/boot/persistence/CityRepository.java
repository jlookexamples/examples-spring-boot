package examples.spring.boot.persistence;

import org.springframework.data.repository.CrudRepository;

import examples.spring.boot.domain.City;

public interface CityRepository extends CrudRepository<City, Long> {
	public City findByName(String name);
}

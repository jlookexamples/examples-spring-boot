package examples.spring.boot.infrastructure.loader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import examples.spring.boot.domain.City;
import examples.spring.boot.persistence.CityRepository;

@Component
public class CityLoader implements ApplicationListener<ContextRefreshedEvent> {
	private static Log LOG = LogFactory.getLog(CityLoader.class);
	
	@Autowired
	private CityRepository repository;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
 		if(LOG.isInfoEnabled()) {
			LOG.info("### ---> onApplicationEvent Starting...");
		}
		
 		City paramus = this.repository.findByName("Paramus");
 		if(paramus == null) {
	 		paramus = new City("Paramus","NJ");
			this.repository.save(paramus);
			
			if(LOG.isInfoEnabled()) {
				LOG.info("Saved Paramus city. - "+paramus);
			}
		}
 		
 		City sanJose = this.repository.findByName("San Jose");
 		if(sanJose == null) {
 		 	sanJose = new City("San Jose","CA");
			this.repository.save(sanJose);
			if(LOG.isInfoEnabled()) {
				LOG.info("Saved San Jose city. - "+sanJose);
			}
 		}
 		
		if(LOG.isInfoEnabled()) {
			LOG.info("### ---> onApplicationEvent Started...");
		}
	}
}

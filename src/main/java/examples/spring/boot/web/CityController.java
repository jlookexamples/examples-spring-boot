package examples.spring.boot.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import examples.spring.boot.domain.City;
import examples.spring.boot.service.CityService;

@RestController
public class CityController {
	
	@Autowired
	private CityService cityService;
	
	@RequestMapping(value="/city/{name}",  method = RequestMethod.GET)
	public City getCity(@PathVariable String name) {
		return this.cityService.getCity(name);
	}
}

package examples.spring.boot.web;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import examples.spring.boot.service.EmpService;

@RestController
public class EmpController {
	private static Log LOG = LogFactory.getLog(EmpController.class);
	
	@Resource(name=EmpService.ID)
	private EmpService service;
	
	@RequestMapping(value="/emp/{id}",  method = RequestMethod.GET)
	public String get(@PathVariable int id) {
		if(LOG.isDebugEnabled()) {
			LOG.debug("emp.get()---------->");
		}
		return service.getEmployee(id);
	}
}

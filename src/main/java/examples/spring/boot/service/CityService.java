package examples.spring.boot.service;

import examples.spring.boot.domain.City;

public interface CityService {
	public static final String ID = "CityService";
	
	public City getCity(String name);
	public City create(City city);
}

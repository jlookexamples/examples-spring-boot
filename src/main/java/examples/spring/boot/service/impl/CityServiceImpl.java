package examples.spring.boot.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import examples.spring.boot.domain.City;
import examples.spring.boot.persistence.CityRepository;
import examples.spring.boot.service.CityService;

@Transactional
@Service(CityService.ID)
public class CityServiceImpl implements CityService {
	private static final Log LOG = LogFactory.getLog(CityServiceImpl.class);
	
	@Autowired
	private CityRepository cityRepository;
	
	@Override
	@Cacheable("myCache")
	@Transactional(readOnly = true)
	public City getCity(String name) {
		if(LOG.isInfoEnabled()) {
			LOG.info("##--> getCity - "+name);
		}
		return this.cityRepository.findByName(name);
	}

	@Override
	@Transactional(readOnly = true)
	public City create(City city) {
		return this.cityRepository.save(city);
	}
}

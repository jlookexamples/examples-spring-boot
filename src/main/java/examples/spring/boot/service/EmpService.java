package examples.spring.boot.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service(EmpService.ID)
public class EmpService {
	public static final String ID = "EmpService";
	private static final Log LOG = LogFactory.getLog(EmpService.class);
	
	@Cacheable("myCache") 
	public String getEmployee(int empId){
		if(LOG.isInfoEnabled()) {
			LOG.info("---Inside getEmployee() Method---");
		}
		
		if(empId==1){			
			return "Shankar";
		}else{
			return "Vishnu";
		}
	}
}

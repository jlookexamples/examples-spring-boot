package examples.spring.boot;


import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.*;

import net.sf.ehcache.management.ManagementService;

@SpringBootApplication
@EnableScheduling
@EnableCaching
@RestController
public class Application {
	
	@Value("${ehcache.config:config/ehcache-org.xml}")
	private String ehcacheConfig;
	
	@RequestMapping("/")
	public String home() {
		return "Hello World!";
	}
	 
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	 
	@Bean
    public CacheManager cacheManager() {
        EhCacheCacheManager cacheManager = new EhCacheCacheManager();
        cacheManager.setCacheManager(ehCacheManagerFactoryBean().getObject());
        
        MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
        ManagementService.registerMBeans(cacheManager.getCacheManager(), mBeanServer, true, true, true, true);
        return cacheManager;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new SimpleKeyGenerator();
    }

    @Bean
    public EhCacheManagerFactoryBean ehCacheManagerFactoryBean() {
        EhCacheManagerFactoryBean ehCacheManagerFactoryBean = new EhCacheManagerFactoryBean();
        ehCacheManagerFactoryBean.setConfigLocation(new ClassPathResource(ehcacheConfig));
        ehCacheManagerFactoryBean.setCacheManagerName(EnvKyes.EHCACHE_MANAGER);
        ehCacheManagerFactoryBean.setShared(true);
        return ehCacheManagerFactoryBean;
    }
}

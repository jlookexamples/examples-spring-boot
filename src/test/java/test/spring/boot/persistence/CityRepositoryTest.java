package test.spring.boot.persistence;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import examples.spring.boot.Application;
import examples.spring.boot.domain.City;
import examples.spring.boot.persistence.CityRepository;

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes=Application.class)
public class CityRepositoryTest {
	
	@Autowired
    private CityRepository repository;
	
//	@Test
	public void findByName() throws Exception {
		String name = "Paramus";
		City paramus = this.repository.findByName(name);
		assertEquals(paramus.getName(), name);
		assertEquals(paramus.getState(), "NJ");
	}
}

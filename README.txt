[Spring Boot Application]

1. RestController 
	- Application.java
	- http://server:port/

2. Spring Data JPA
	- City.java, CityRepository.java, CityService.java, CityServiceImpl.java, CityController.java
	- Data Loading : CityLoader.java
	- http://server:port/city/Paramus

3. ehcache 
	- CityServiceImpl.java, CityController.java
	- http://server:port/city/Paramus
	- Cache Replication : config/ehcache-prd1.xml, config/ehcache-prd2.xml
		run instance-1 : gradle run -Pprofile=prd -Pinstance=1
		run instance-2 : gradle run -Pprofile=prd -Pinstance=2
   	- http://www.ehcache.org/documentation/2.8/replication/rmi-replicated-caching.html

4. Run
	- Default Environment : gradle run
	- Development Environment : gradle run -Dprofile=dev
	- Production Environment : gradle run -Dprofile=prd -Dinstance=1, gradle run -Dprofile=prd -Dinstance=2

5. Reference : http://www.slideshare.net/hsjeon70/spring-boot-62365795